/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../styles/home.scss';

// start the Stimulus application
import '../bootstrap';

const work = document.getElementById('work');
const arrow = document.getElementById('arrow');
work.style.display = "none";
arrow.addEventListener('click', function(){
    if(getComputedStyle(work).display != "none"){
        work.style.display = "none";
    } else {
        work.style.display = "block";
    }
})