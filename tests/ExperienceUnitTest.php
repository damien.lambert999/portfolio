<?php

namespace App\Tests;

use App\Entity\Experience;
use App\Entity\Technology;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ExperienceUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $experience = new Experience();
        $date = new DateTime();
        $user = new User();

        $experience->setWorkplace('workplace')
                ->setCity('city')
                ->setSociety('society')
                ->setStartDate($date)
                ->setEndDate($date)
                ->setIsActual(true)
                ->setUser($user);
        
        $this->assertTrue($experience->getWorkplace() === 'workplace');
        $this->assertTrue($experience->getCity() === 'city');
        $this->assertTrue($experience->getSociety() === 'society');
        $this->assertTrue($experience->getStartDate() === $date);
        $this->assertTrue($experience->getEndDate() === $date);
        $this->assertTrue($experience->getIsActual() === true);
        $this->assertTrue($experience->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $experience = new Experience();
        $date = new DateTime();
        $user = new User();

        $experience->setWorkplace('workplace')
                ->setCity('city')
                ->setSociety('society')
                ->setStartDate($date)
                ->setEndDate($date)
                ->setIsActual(true)
                ->setUser($user);
        
        $this->assertFalse($experience->getWorkplace() === 'false');
        $this->assertFalse($experience->getCity() === 'false');
        $this->assertFalse($experience->getSociety() === 'false');
        $this->assertFalse($experience->getStartDate() === new DateTime());
        $this->assertFalse($experience->getEndDate() === new DateTime());
        $this->assertFalse($experience->getIsActual() === false);
        $this->assertFalse($experience->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $experience = new Experience();
        
        
        $this->assertEmpty($experience->getWorkplace());
        $this->assertEmpty($experience->getCity());
        $this->assertEmpty($experience->getSociety());
        $this->assertEmpty($experience->getStartDate());
        $this->assertEmpty($experience->getEndDate());
        $this->assertEmpty($experience->getIsActual());
        $this->assertEmpty($experience->getUser());
        $this->assertEmpty($experience->getId());
    }

    public function testAddGetRemoveTechnology()
    {
        $experience = new Experience();
        $technology = new Technology();

        $this->assertEmpty($experience->getTechnologies());

        $experience->addTechnology($technology);
        $this->assertContains($technology, $experience->getTechnologies());

        $experience->removeTechnology($technology);
        $this->assertEmpty($experience->getTechnologies());
    }
}
