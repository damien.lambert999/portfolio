<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Hobby;
use PHPUnit\Framework\TestCase;

class HobbyUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $hobby = new Hobby();
        $user = new User();

        $hobby->setName('name')
            ->setUser($user);

        $this->assertTrue($hobby->getName() === 'name');
        $this->assertTrue($hobby->getUser() === $user);

    }

    public function testIsFalse(): void
    {
        $hobby = new Hobby();
        $user = new User();

        $hobby->setName('name')
            ->setUser($user);

        $this->assertFalse($hobby->getName() === 'false');
        $this->assertFalse($hobby->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $hobby = new Hobby();

        $this->assertEmpty($hobby->getName());
        $this->assertEmpty($hobby->getUser());
        $this->assertEmpty($hobby->getId());
    }
}
