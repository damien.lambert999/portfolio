<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Asset;
use PHPUnit\Framework\TestCase;

class AssetUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $asset = new Asset();
        $user = new User();

        $asset->setName('name')
            ->setUser($user);

        $this->assertTrue($asset->getName() === 'name');
        $this->assertTrue($asset->getUser() === $user);

    }

    public function testIsFalse(): void
    {
        $asset = new Asset();
        $user = new User();

        $asset->setName('name')
            ->setUser($user);

        $this->assertFalse($asset->getName() === 'false');
        $this->assertFalse($asset->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $asset = new Asset();

        $this->assertEmpty($asset->getName());
        $this->assertEmpty($asset->getUser());
        $this->assertEmpty($asset->getId());
    }
}