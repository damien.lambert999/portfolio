<?php

namespace App\Tests;

use App\Entity\Skill;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class SkillUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $skill = new Skill();
        $user = new User();

        $skill->setName('name')
            ->setUser($user);

        $this->assertTrue($skill->getName() === 'name');
        $this->assertTrue($skill->getUser() === $user);

    }

    public function testIsFalse(): void
    {
        $skill = new Skill();
        $user = new User();

        $skill->setName('name')
            ->setUser($user);

        $this->assertFalse($skill->getName() === 'false');
        $this->assertFalse($skill->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $skill = new Skill();

        $this->assertEmpty($skill->getName());
        $this->assertEmpty($skill->getUser());
        $this->assertEmpty($skill->getId());
    }
}
