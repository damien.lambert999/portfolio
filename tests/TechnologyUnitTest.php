<?php

namespace App\Tests;

use App\Entity\Experience;
use App\Entity\Technology;
use PHPUnit\Framework\TestCase;

class TechnologyUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $technology = new Technology();
        $experience = new Experience();

        $technology->setName('name')
                ->setExperience($experience);

        $this->assertTrue($technology->getName() === 'name');
        $this->assertTrue($technology->getExperience() === $experience);
    }

    public function testIsFalse(): void
    {
        $technology = new Technology();
        $experience = new Experience();

        $technology->setName('name')
                ->setExperience($experience);

        $this->assertFalse($technology->getName() === 'false');
        $this->assertFalse($technology->getExperience() === new Experience());
    }

    public function testIsEmpty(): void
    {
        $technology = new Technology();

        $this->assertEmpty($technology->getName());
        $this->assertEmpty($technology->getExperience());
        $this->assertEmpty($technology->getId());
    }
}
