<?php

namespace App\Tests;

use App\Entity\Formation;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class FormationUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $formation = new Formation();
        $user = new User();
        $date = new DateTime();

        $formation->setName('name')
                ->setSchool('school')
                ->setCity('city')
                ->setStartDate($date)
                ->setEndDate($date)
                ->setIsActual(true)
                ->setDescription('texte de test')
                ->setUser($user);

        $this->assertTrue($formation->getName() === 'name');
        $this->assertTrue($formation->getSchool() === 'school');
        $this->assertTrue($formation->getCity() === 'city');
        $this->assertTrue($formation->getStartDate() === $date);
        $this->assertTrue($formation->getEndDate() === $date);
        $this->assertTrue($formation->getIsActual() === true);
        $this->assertTrue($formation->getDescription() === 'texte de test');
        $this->assertTrue($formation->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $formation = new Formation();
        $user = new User();
        $date = new DateTime();

        $formation->setName('name')
                ->setSchool('school')
                ->setCity('city')
                ->setStartDate($date)
                ->setEndDate($date)
                ->setIsActual(true)
                ->setDescription('texte de test')
                ->setUser($user);

        $this->assertFalse($formation->getName() === 'false');
        $this->assertFalse($formation->getSchool() === 'false');
        $this->assertFalse($formation->getCity() === 'false');
        $this->assertFalse($formation->getStartDate() === new DateTime());
        $this->assertFalse($formation->getEndDate() === new DateTime());
        $this->assertFalse($formation->getIsActual() === false);
        $this->assertFalse($formation->getDescription() === 'false');
        $this->assertFalse($formation->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $formation = new Formation();

        $this->assertEmpty($formation->getName());
        $this->assertEmpty($formation->getSchool());
        $this->assertEmpty($formation->getCity());
        $this->assertEmpty($formation->getStartDate());
        $this->assertEmpty($formation->getEndDate());
        $this->assertEmpty($formation->getIsActual());
        $this->assertEmpty($formation->getDescription());
        $this->assertEmpty($formation->getUser());
        $this->assertEmpty($formation->getId());
    }
}
