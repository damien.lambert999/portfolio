<?php

namespace App\Tests;

use App\Entity\Asset;
use App\Entity\Experience;
use App\Entity\Formation;
use App\Entity\Hobby;
use App\Entity\Skill;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $date = new DateTime();

        $user->setEmail('true@test.com')
            ->setFirstname('prenom')
            ->setLastname('nom')
            ->setPassword('password')
            ->setBirthdate($date)
            ->setDescription('texte de test')
            ->setPhoto('lien-photo')
            ->setLinkedIn('lien-linkedIn');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstname() === 'prenom');
        $this->assertTrue($user->getLastname() === 'nom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getBirthdate() === $date);
        $this->assertTrue($user->getDescription() === 'texte de test');
        $this->assertTrue($user->getPhoto() === 'lien-photo');
        $this->assertTrue($user->getLinkedIn() === 'lien-linkedIn');
    }

    public function testIsFalse(): void
    {
        $user = new User();
        $date = new DateTime();

        $user->setEmail('true@test.com')
            ->setFirstname('prenom')
            ->setLastname('nom')
            ->setPassword('password')
            ->setBirthdate($date)
            ->setDescription('texte de test')
            ->setPhoto('lien-photo')
            ->setLinkedIn('lien-linkedIn');

        $this->assertFalse($user->getEmail() === 'false');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getBirthdate() === new DateTime());
        $this->assertFalse($user->getDescription() === 'false');
        $this->assertFalse($user->getPhoto() === 'false');
        $this->assertFalse($user->getLinkedIn() === 'false');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getBirthdate());
        $this->assertEmpty($user->getDescription());
        $this->assertEmpty($user->getPhoto());
        $this->assertEmpty($user->getLinkedIn());
        $this->assertEmpty($user->getId());

    }

    public function testAddGetRemoveExperience()
    {
        $user = new User();
        $experience = new Experience();

        $this->assertEmpty($user->getExperiences());

        $user->addExperience($experience);
        $this->assertContains($experience, $user->getExperiences());

        $user->removeExperience($experience);
        $this->assertEmpty($user->getExperiences());
    }

    public function testAddGetRemoveFormation()
    {
        $user = new User();
        $formation = new Formation();

        $this->assertEmpty($user->getFormations());

        $user->addFormation($formation);
        $this->assertContains($formation, $user->getFormations());

        $user->removeFormation($formation);
        $this->assertEmpty($user->getFormations());
    }

    public function testAddGetRemoveSkill()
    {
        $user = new User();
        $skill = new Skill();

        $this->assertEmpty($user->getSkills());

        $user->addSkill($skill);
        $this->assertContains($skill, $user->getSkills());

        $user->removeSkill($skill);
        $this->assertEmpty($user->getSkills());
    }

    public function testAddGetRemoveAsset()
    {
        $user = new User();
        $asset = new Asset();

        $this->assertEmpty($user->getAssets());

        $user->addAsset($asset);
        $this->assertContains($asset, $user->getAssets());

        $user->removeAsset($asset);
        $this->assertEmpty($user->getAssets());
    }

    public function testAddGetRemoveHobby()
    {
        $user = new User();
        $hobby = new Hobby();

        $this->assertEmpty($user->getHobbies());

        $user->addHobby($hobby);
        $this->assertContains($hobby, $user->getHobbies());

        $user->removeHobby($hobby);
        $this->assertEmpty($user->getHobbies());
    }
}
