<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user->setEmail('damien.lambert999@gmail.com')
            ->setFirstname('Damien')
            ->setLastname('Lambert')
            ->setBirthdate(new DateTime('1982-10-02'))
            ->setDescription('Après avoir passé plus de 15 ans en tant que vendeur en grande distribution, et suite à une
            reconversion professionnelle, étant passionné d’informatique, je me suis orienté vers le
            développement web.')
            ->setRoles(['ROLE_ADMIN'])
            ->setLinkedIn('https://www.linkedin.com/in/damien-lambert-98b2b01b8/');
        
        $password = $this->encoder->encodePassword($user, 'Aze123qw%');
        $user->setPassword($password);
        
        $manager->persist($user);

        $manager->flush();
    }
}
